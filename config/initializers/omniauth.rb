Rails.application.config.middleware.use OmniAuth::Builder do
  provider :gitlab, ENV['GITLAB_KEY'], ENV['GITLAB_SECRET'],
           client_options: {
               site: 'http://localhost:3000'
           }
end
